# Icons

This repository contains the base Photoshop File (`.psd`) for generating the rest of icons, and the sized icons that will be served in the server. It also contains the Progressive Web App manifest (`manifest.json`) for some weird reason.
